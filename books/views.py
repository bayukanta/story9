from django.shortcuts import render
from django.http import JsonResponse
import urllib.request
import json
# Create your views here.


def data(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
    req = urllib.request.Request(url)

    read = urllib.request.urlopen(req).read()
    my_dict = json.loads(read.decode('utf-8'))
    return JsonResponse(my_dict)


def index(request):
    return render(request, "landing.html")
    