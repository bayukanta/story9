from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views

class Story9UnitTest(TestCase):
    def test_url_landing_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landing.html')

    def test_url_data_is_exist(self):
        response = Client().get('/data')
        self.assertEqual(response.status_code, 200)

    def test_function_data(self):
        found = resolve('/data')
        self.assertEqual(found.func, views.data)
